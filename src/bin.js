#!/usr/bin/env node

const { basename } = require('path')

const {
  convertSample,
  createBin,
  getInfo
} = require('./index.js')

var argv = require('minimist')(process.argv.slice(2), {
  alias: {
    o: 'out'
  },
  string: 'o'
});

if (!argv.out || !argv._.length){
  console.error('Usage: tomdrum -o out.bin wave1.wav wave2.wav...')
  process.exit(1)
}

try{
  // TODO: check total time-length < 1.43 seconds
  argv._.forEach(w => {
    const i = getInfo(w)
    console.log(`Converting ${basename(w)}: ${i.channels}-channel, ${i.samplerate}, ${i.format.join(', ')}`)
  })
  createBin(argv._, argv.out)
}catch(e){
  console.error(e.message)
  process.exit(1)
}