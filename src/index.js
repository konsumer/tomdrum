const { tomdrum, info } = require('bindings')('tomdrum')
const { tempdir, rm } = require('shelljs')
const { readFileSync, writeFileSync } = require('fs')

const temp = tempdir()

const flags = {
  /* Major formats. */
  'Microsoft WAV format (little endian)': 0x010000, // FORMAT_WAV
  'Apple/SGI AIFF format (big endian)': 0x020000, // FORMAT_AIFF
  'Sun/NeXT AU format (big endian)': 0x030000, // FORMAT_AU
  'RAW PCM data': 0x040000, // FORMAT_RAW
  'Ensoniq PARIS file format': 0x050000, // FORMAT_PAF
  'Amiga IFF / SVX8 / SV16 format. ': 0x060000, // FORMAT_SVX
  'Sphere NIST format': 0x070000, // FORMAT_NIST
  'VOC files': 0x080000, // FORMAT_VOC
  'Berkeley/IRCAM/CARL ': 0x0A0000, // FORMAT_IRCAM
  'Sonic Foundry\'s 64 bit RIFF/WAV': 0x0B0000, // FORMAT_W64
  'Matlab (tm) V4.2 / GNU Octave 2.0': 0x0C0000, // FORMAT_MAT4
  'Matlab (tm) V5.0 / GNU Octave 2.1': 0x0D0000, // FORMAT_MAT5
  'Portable Voice Format ': 0x0E0000, // FORMAT_PVF
  'Fasttracker 2 Extended Instrument': 0x0F0000, // FORMAT_XI
  'HMM Tool Kit format': 0x100000, // FORMAT_HTK
  'Midi Sample Dump Standard': 0x110000, // FORMAT_SDS
  'Audio Visual Research': 0x120000, // FORMAT_AVR
  'MS WAVE with WAVEFORMATEX': 0x130000, // FORMAT_WAVEX
  'Sound Designer 2': 0x160000, // FORMAT_SD2
  'FLAC lossless file format': 0x170000, // FORMAT_FLAC
  'Core Audio File format': 0x180000, // FORMAT_CAF
  'Psion WVE format': 0x190000, // FORMAT_WVE
  'Xiph OGG container': 0x200000, // FORMAT_OGG
  'Akai MPC 2000 sampler': 0x210000, // FORMAT_MPC2K
  'RF64 WAV file': 0x220000, // FORMAT_RF64

  /* Subtypes from here on. */

  'Signed 8 bit data': 0x0001, // FORMAT_PCM_S8
  'Signed 16 bit data': 0x0002, // FORMAT_PCM_16
  'Signed 24 bit data': 0x0003, // FORMAT_PCM_24
  'Signed 32 bit data': 0x0004, // FORMAT_PCM_32

  'Unsigned 8 bit data (WAV and RAW only)': 0x0005, // FORMAT_PCM_U8

  '32 bit float data': 0x0006, // FORMAT_FLOAT
  '64 bit float data': 0x0007, // FORMAT_DOUBLE

  'U-Law encoded': 0x0010, // FORMAT_ULAW
  'A-Law encoded': 0x0011, // FORMAT_ALAW
  'IMA ADPCM': 0x0012, // FORMAT_IMA_ADPCM
  'Microsoft ADPCM': 0x0013, // FORMAT_MS_ADPCM

  'GSM 6.10 encoding': 0x0020, // FORMAT_GSM610
  'Oki Dialogic ADPCM encoding': 0x0021, // FORMAT_VOX_ADPCM

  '32kbs G721 ADPCM encoding': 0x0030, // FORMAT_G721_32
  '24kbs G723 ADPCM encoding': 0x0031, // FORMAT_G723_24
  '40kbs G723 ADPCM encoding': 0x0032, // FORMAT_G723_40

  '12 bit Delta Width Variable Word encoding': 0x0040, // FORMAT_DWVW_12
  '16 bit Delta Width Variable Word encoding': 0x0041, // FORMAT_DWVW_16
  '24 bit Delta Width Variable Word encoding': 0x0042, // FORMAT_DWVW_24
  'N bit Delta Width Variable Word encoding': 0x0043, // FORMAT_DWVW_N

  '8 bit differential PCM (XI only)': 0x0050, // FORMAT_DPCM_8
  '16 bit differential PCM (XI only)': 0x0051, // FORMAT_DPCM_16

  'Xiph Vorbis encoding': 0x0060, // FORMAT_VORBIS

  /* Endian-ness options. */

  'Default file endian-ness': 0x00000000, // ENDIAN_FILE
  'Force little endian-ness': 0x10000000, // ENDIAN_LITTLE
  'Force big endian-ness': 0x20000000, // ENDIAN_BIG
  'Force CPU endian-ness': 0x30000000 // ENDIAN_CPU
}

// convert to 8-bit uLaw, 22.7 KHz, strip first 44 bytes (WAV header)
const convertSample = file => {
  const t = `${temp}/${Date.now()}.wav`
  tomdrum(file, t)
  const f = readFileSync(t).slice(44)
  rm(t)
  return f
}

// get info about a WAV file
const getInfo = file => {
  const i = info(file)
  const format = []
  Object.keys(flags).forEach(f => {
    if ((flags[f] & i.format) === flags[f]) {
      format.push(f)
    }
  })
  return {...i, format}
}

// get 4 4bit nums from a 16bit number
// this should probly be replaced with bit-math
const address = n => {
  const v = n.toString(2).padStart(16, '0').split('').map(v => parseInt(v))
  return [
    parseInt(v.slice(0, 3).join(''), 2),
    parseInt(v.slice(4, 7).join(''), 2),
    parseInt(v.slice(8, 11).join(''), 2),
    parseInt(v.slice(12, 15).join(''), 2)
  ]
}

// get next address divisible by 32
const next = a => (a % 32) === 0 ? a : a - (a % 32) + 32

// create a complete TOM ROM file
const createBin = (samples, outfile) => {
  if (samples.length > 7) {
    throw new Error('Must use 7 samples or less')
  }
  const out = new Buffer(32768)
  out.writeUInt8(0x05, 3)
  out.writeUInt8(0x0A, 5)
  out.writeUInt8(samples.length, 7)
  let last = 256
  let first = 256
  samples
    .map(convertSample)
    .forEach((sample, i) => {
      last = next(first + sample.length)
      const f = address(first)
      const l = address(last)

      out.writeUInt8(f[0], 18 * (i + 1))
      out.writeUInt8(f[1], (18 * (i + 1)) + 2)
      out.writeUInt8(f[2], (18 * (i + 1)) + 4)
      out.writeUInt8(f[3], (18 * (i + 1)) + 6)

      out.writeUInt8(l[0], (18 * (i + 1)) + 8)
      out.writeUInt8(l[1], (18 * (i + 1)) + 10)
      out.writeUInt8(l[2], (18 * (i + 1)) + 12)
      out.writeUInt8(l[3], (18 * (i + 1)) + 14)

      for (let s = 0; s < sample.length; s++) {
        out[first + s] = sample[s]
      }
      first = last + 1
    })
  writeFileSync(outfile, out)
}

module.exports = {
  convertSample,
  createBin,
  getInfo
}
