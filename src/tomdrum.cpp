#include <iostream>
#include <nan.h>
#include <sndfile.h>

using namespace std;
using namespace v8;
using namespace Nan;

// TODO: this uses 2 files, eventually it should just return a Buffer

// turn an audio file into a WAV file: 8-bit uLaw, 22.7 KHz (with 44 byte header)
NAN_METHOD(tomdrum) {
  Utf8String arg0(info[0]);
  char *sndfilenameIn = *arg0;
  Utf8String arg1(info[1]);
  char *sndfilenameOut = *arg1;

  char msg [100];
  SNDFILE* sndfile;
  SF_INFO sfinfo;
  sndfile = sf_open(sndfilenameIn, SFM_READ, &sfinfo);

  if (sndfile == 0) {
    sprintf (msg, "Not able to open input file %s.", sndfile);
    ThrowError(msg);
    return;
  }

  float *audioIn = new float[sfinfo.channels * sfinfo.frames];
  sf_read_float(sndfile, audioIn, sfinfo.channels * sfinfo.frames);
  
  // mixdown
  float *audioOut = new float[sfinfo.frames];
  for (int i = 0; i < sfinfo.frames; i++) {
    audioOut[i] = 0;
    for (int j = 0; j < sfinfo.channels; j++) {
      audioOut[i] += audioIn[i*sfinfo.channels + j];
    }
    audioOut[i] /= sfinfo.channels;
  }
  sf_close(sndfile);
  
  // write output
  int frames = sfinfo.frames;    
  sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_ULAW;
  sfinfo.channels = 1;
  sfinfo.samplerate = 22700;
  sndfile = sf_open(sndfilenameOut, SFM_WRITE, &sfinfo);
  sf_write_float(sndfile, audioOut, frames);
  sf_close(sndfile);
  
  // free memory
  delete[] audioIn;
  delete[] audioOut;
}

// get info about an audio file
NAN_METHOD(info) {
  Utf8String arg0(info[0]);
  char *sndfilenameIn = *arg0;
  
  char msg [100];
  SNDFILE* sndfile;
  SF_INFO sfinfo;
  sndfile = sf_open(sndfilenameIn, SFM_READ, &sfinfo);

  if (sndfile == 0) {
    sprintf (msg, "Not able to open input file %s.", sndfile);
    ThrowError(msg);
    return;
  }

  sf_close(sndfile);

  Local<Object> retval = Nan::New<Object>();
  Nan::Set(retval,
    Nan::New<String>("frames").ToLocalChecked(),
    Nan::New<Number>(sfinfo.frames)
  );
  Nan::Set(retval,
    Nan::New<String>("channels").ToLocalChecked(),
    Nan::New<Number>(sfinfo.channels)
  );
  Nan::Set(retval,
    Nan::New<String>("samplerate").ToLocalChecked(),
    Nan::New<Number>(sfinfo.samplerate)
  );
  Nan::Set(retval,
    Nan::New<String>("format").ToLocalChecked(),
    Nan::New<Number>(sfinfo.format)
  );
  Nan::Set(retval,
    Nan::New<String>("sections").ToLocalChecked(),
    Nan::New<Number>(sfinfo.sections)
  );
  
  info.GetReturnValue().Set(retval);
}

NAN_MODULE_INIT(Init) {
  Nan::Set(target,
    New<String>("tomdrum").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(tomdrum)).ToLocalChecked()
  );
  Nan::Set(target,
    New<String>("info").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(info)).ToLocalChecked()
  );
}

NODE_MODULE(tomdrum, Init)